package upv.com.practica4;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ActionBar ab=getActionBar();
        //ab.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent operacion;

        switch(item.getItemId())
        {
            case R.id.operacion1:
                operacion=new Intent(this, Temperatura.class);
                startActivity(operacion);
                return true;
            case R.id.operacion2:
                operacion=new Intent(this, Area.class);
                startActivity(operacion);
                return true;
            case R.id.operacion3:
                operacion=new Intent(this, Distancia.class);
                startActivity(operacion);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
